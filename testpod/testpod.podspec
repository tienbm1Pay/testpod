Pod::Spec.new do |s|
s.platform = :ios, '9.0'
s.name = "testpod"
s.summary="base library"
s.requires_arc = true
s.version = "0.1.0"
s.authors= { 'TienBM' => 'tienbm@truemoney.com.vn' }
s.license={ :type => 'BSD' }
s.homepage="https://tienbm1Pay@bitbucket.org/tienbm1Pay/testpod.git"
s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.0' }
s.source= { :git => "https://tienbm1Pay@bitbucket.org/tienbm1Pay/testpod.git", :tag => s.version }
s.source_files = 'testpod/**/*.{h,m,swift}'
s.resources = 'testpod/**/*.{storyboard,xib,strings,xcassets}'
s.exclude_files = 'testpod/**/{AppDelegate.swift,LaunchScreen.xib}'
end
