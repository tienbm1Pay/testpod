APP_NAME=`pwd | awk -F/ '{print $NF}'`
RELEASE=$(/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" "`pwd`/$APP_NAME/Info.plist")

POD_SPEC_REPO_NAME="testpodspec"
POD_SPEC_REPO="https://tienbm1Pay@bitbucket.org/tienbm1Pay/$POD_SPEC_REPO_NAME.git"
LIBRARY_REPO="https://tienbm1Pay@bitbucket.org/tienbm1Pay/$APP_NAME.git"
VERSION="0.1.0"

function createLibrary {

#create new podspec
	rm $APP_NAME.podspec
	touch $APP_NAME.podspec
	echo "Pod::Spec.new do |s|" >> $APP_NAME.podspec
	echo "s.platform = :ios, '9.0'" >> $APP_NAME.podspec
	echo "s.name = \"$APP_NAME\"" >> $APP_NAME.podspec	
	echo "s.summary=\"base library\"" >> $APP_NAME.podspec
	echo "s.requires_arc = true" >> $APP_NAME.podspec
	# echo "s.version = \"$RELEASE\"" >> $APP_NAME.podspec
	echo "s.version = \"0.1.0\"" >> $APP_NAME.podspec
	echo "s.authors= { 'TienBM' => 'tienbm@truemoney.com.vn' }" >> $APP_NAME.podspec
	echo "s.license={ :type => 'BSD' }" >> $APP_NAME.podspec
	echo "s.homepage=\"$LIBRARY_REPO\"" >> $APP_NAME.podspec
	echo "s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.0' }" >> $APP_NAME.podspec
	echo "s.source= { :git => \"$LIBRARY_REPO\", :tag => s.version }" >> $APP_NAME.podspec
	echo "s.source_files = '$APP_NAME/**/*.{h,m,swift}'" >> $APP_NAME.podspec
	echo "s.resources = '$APP_NAME/**/*.{storyboard,xib,strings,xcassets}'" >> $APP_NAME.podspec
	echo "s.exclude_files = '$APP_NAME/**/{AppDelegate.swift,LaunchScreen.xib}'" >> $APP_NAME.podspec
#    echo "s.dependency 'ami-ios-base'" >> $APP_NAME.podspec
	echo "end" >> $APP_NAME.podspec

	git add -A
	git commit -m "update library to version: $RELEASE"
	git push -u origin master
	
	git tag "$VERSION"
	git push origin --tags
	
	pod repo push $POD_SPEC_REPO_NAME $APP_NAME.podspec --allow-warnings
}

pod repo add $POD_SPEC_REPO_NAME $POD_SPEC_REPO

createLibrary
